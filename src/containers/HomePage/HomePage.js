/** @jsx jsx */
import { useState } from 'react';
import { Row, Col } from 'react-grid-system';
import Circle from 'components/Circle';
import logo from 'assets/logo-circles.svg';
import { jsx } from '@emotion/react';
import { styles } from './HomePage.css';

import { getRandomIndicators } from 'mocks/indicators';
import { getIpsumTitle } from 'mocks/ipsum';
import mockup from 'mocks/mockup.png';

const HomePage = () => {
  const [ randomTitle, setRandomTitle ] = useState(getIpsumTitle());
  const [ randomIndicators, setRandomIndicators ] = useState(getRandomIndicators(2, 8));

  const handleClick = () => {
    setRandomTitle(getIpsumTitle());
    setRandomIndicators(getRandomIndicators(2, 8));
  };

  return (
    <section css={styles}>
      <Row align="start" justify="around">
        <Col xs={12}>
          <header>
            <h1>
              <img src={logo} alt="Circles" /> Frontend Test
            </h1>
            <hr />
          </header>
        </Col>
        <Col xs={12} sm={12} md={6}>
          <h2>Source Example</h2>
          <h3>PNG asset export from Sketch</h3>
          <img src={mockup} className="dev-mockup" />
        </Col>
        <Col xs={12} sm={12} md={6}>
          <h2>Circle Component</h2>
          <h3>RANDOM name + indicators array</h3>
          {randomIndicators && <Circle title={randomTitle} indicators={randomIndicators} />}
          <a onClick={() => handleClick()}>randomize!</a>
        </Col>
        <Col xs={12}>
          <div className="data">
            {randomTitle && `title: "${randomTitle}"`}
            <br />
            {randomIndicators && `indicators: ${JSON.stringify(randomIndicators)}`}
          </div>
        </Col>
      </Row>
    </section>
  );
};

export default HomePage;
