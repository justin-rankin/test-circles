import { css } from '@emotion/react';

export const styles = css`
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: flex-start;

  & > div {
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    max-width: 1000px;
  }

  & > div > div {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
  }

  header {
    width: 100%;
    max-width: 90vw;
    padding: 0 2em;
    text-align: center;

    h1 {
      color: white;
      img {
        filter: invert(1);
        display: inline-block;
        height: 0.8em;
        vertical-align: middle;
        margin: 0em 0.3em 0.22em 0.3em;
      }
    }
    hr {
      border-color: rgba(255, 255, 255, 0.2);
      margin: 0 auto;
      width: 100%;
    }
  }

  h2 {
    color: rgba(255, 255, 255, 0.8);
    font-size: 1.2em;
    margin-top: 3em;
    margin-bottom: 0;
  }

  h3 {
    color: rgba(255, 255, 255, 0.4);
    font-size: 0.9em;
    margin-top: 0;
  }

  a {
    font-size: 0.9em;
    z-index: 50;
    color: white !important;
    border: 1px solid white;
    padding: 0.33em 0.66em;
    margin: 1em 1em 3em;
    border-radius: 0.5em;
    cursor: pointer;
    background-color: rgba(255, 255, 255, 0.1);
    transition: background-color 300ms ease;
    &:hover {
      background-color: rgba(255, 255, 255, 0.3);
    }
  }

  .dev-mockup {
    margin: 20px;
    width: 220px;
    margin: 10px;
  }

  .data {
    font-family: monospace;
    color: rgba(255, 255, 255, 0.8);
    text-align: center;
    margin: 5px 100px 50px 100px;
  }
`;
