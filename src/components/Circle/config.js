export const circumference = 100; // PERCENT
export const diameterPX = 240;
export const diameter = 40;
export const rValue = 15.91549430918954;

export const rotation = 135;

export const radius = diameter / 2;
export const cx = radius;
export const cy = radius;

export const adjustPX = diameterPX * 0.104;
export const indicatorWidth = 2;
export const breakWidth = 1;

export const ringPadding = 3;
export const ringWidth = indicatorWidth + ringPadding;

export const innerScale = 0.78;
export const innerRingWidth = 3.3;

const propsSpacingMaskSVG = {
  points: `${diameter} ${diameter / 2} ${diameter / 2} ${diameter / 2}`,
  strokeWidth: breakWidth,
  stroke: 'black',
};

// PROPS - OUTER ELEMENTS:

export const propsOuterSVG = {
  cx,
  cy,
  r: rValue,
  fill: 'transparent',
};

export const getPropsOuterMask = (indicators) => {
  return {
    indicators,
    rotation: -rotation,
    propsSpacingMaskSVG,
  };
};

export const getPropsOuterRing = (indicators) => {
  return {
    indicators,
    ringWidth,
    rotation: -rotation,
    propsOuterSVG,
  };
};

// PROPS - INNER ELEMENTS:

export const propsInnerSVG = {
  cx,
  cy,
  r: rValue * innerScale,
  fill: 'transparent',
  strokeWidth: ringWidth * innerRingWidth,
};

export const propsInnerMask = {
  diameter,
  rValue,
  innerScale,
  propsSpacingMaskSVG,
};

export const propsInnerCircle = {
  diameter,
  rValue,
  ringPadding,
};

export const propsInnerTitle = {
  diameter,
  ringPadding,
  rValue,
  innerScale,
  propsInnerSVG,
};
