/** @jsx jsx */
import PropTypes from 'prop-types';
import CircleOuter from 'components/CircleOuter';
import CircleInner from 'components/CircleInner';
import { jsx } from '@emotion/react';
import { styles } from './Circle.css';

const Circle = ({ title, indicators }) => {
  return (
    <div css={styles}>
      <CircleOuter indicators={indicators} />
      <CircleInner title={title} />
    </div>
  );
};

Circle.propTypes = {
  title: PropTypes.string.isRequired,
  indicators: PropTypes.array.isRequired,
};

export default Circle;
