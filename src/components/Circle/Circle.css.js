import { css } from '@emotion/react';
import { diameterPX } from 'components/Circle/config';

export const styles = css`
  width: ${diameterPX}px;
  height: ${diameterPX}px;
`;
