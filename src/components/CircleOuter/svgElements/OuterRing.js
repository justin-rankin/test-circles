/** @jsx jsx */
import PropTypes from 'prop-types';
import Indicator from '../components/Indicator';
import { jsx } from '@emotion/react';

const OuterRing = ({ indicators, ringWidth, rotation, propsOuterSVG }) => {
  return (
    <g>
      <circle className="outer-ring-bg" {...propsOuterSVG} strokeWidth={ringWidth} />
      <g mask="url(#maskBreaks)" style={{ transformOrigin: 'center', transform: `rotate(${rotation}deg)` }}>
        {indicators.map((data, i) => {
          return <Indicator data={data} numSegs={indicators.length} index={i} key={i} />;
        })}
      </g>
    </g>
  );
};

OuterRing.propTypes = {
  indicators: PropTypes.array.isRequired,
  ringWidth: PropTypes.number.isRequired,
  rotation: PropTypes.number.isRequired,
  propsOuterSVG: PropTypes.object.isRequired,
};

export default OuterRing;
