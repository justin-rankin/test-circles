/** @jsx jsx */
import PropTypes from 'prop-types';
import { jsx } from '@emotion/react';

const OuterMask = ({ indicators, rotation, propsSpacingMaskSVG }) => {
  return (
    <mask id="maskBreaks" style={{ transformOrigin: 'center', transform: `rotate(${rotation}deg)` }}>
      <rect width="100%" height="100%" fill="white" />
      {indicators.map((data, i) => {
        const angle = Math.floor(360 / indicators.length) * i;
        // START-POS = 3 O'CLOCK / OFFSET: 0
        // TRANSFORM SETS START-POS to 12 O'CLOCL
        return (
          <polyline
            {...propsSpacingMaskSVG}
            style={{ transformOrigin: 'center', transform: `rotate(${angle}deg)` }}
            key={i}
          />
        );
      })}
    </mask>
  );
};

OuterMask.propTypes = {
  indicators: PropTypes.array.isRequired,
  rotation: PropTypes.number.isRequired,
  propsSpacingMaskSVG: PropTypes.object.isRequired,
};

export default OuterMask;
