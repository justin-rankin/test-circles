import { css } from '@emotion/react';
import { colors } from 'styles';

export const styles = css`
    &.indicator {
      &.state-unlit {
        stroke: ${colors.states.unlit};
      }
      &.state-lit {
        stroke: ${colors.states.lit};
      }
      &.state-broken {
        stroke: ${colors.states.broken};
      }
    }
  }
`;
