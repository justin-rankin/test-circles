/** @jsx jsx */
import PropTypes from 'prop-types';
import { jsx } from '@emotion/react';
import { styles } from './Indicator.css';

import { circumference, indicatorWidth, propsOuterSVG } from 'components/Circle/config';

const Indicator = ({ data, numSegs, index }) => {
  const segSize = circumference / numSegs;
  const { state } = data;

  return (
    <g>
      <circle
        id={`indicator-${index}`}
        css={styles}
        className={`indicator state-${state}`}
        {...propsOuterSVG}
        strokeWidth={indicatorWidth}
        strokeDasharray={`${segSize} 0 0 ${circumference - segSize}`}
        strokeDashoffset={segSize * index}
      />
    </g>
  );
};

Indicator.propTypes = {
  data: PropTypes.object.isRequired,
  numSegs: PropTypes.number.isRequired,
  index: PropTypes.number.isRequired,
};

export default Indicator;
