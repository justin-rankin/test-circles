/** @jsx jsx */
import PropTypes from 'prop-types';
import iconBreak from 'assets/iconBroken.svg';
import { jsx } from '@emotion/react';
import { styles } from './IconsBroken.css';

import { diameterPX, adjustPX, rotation } from 'components/Circle/config';

const getRingCoords = (angle) => {
  const radius = diameterPX / 2 - adjustPX;
  const cx = radius;
  const cy = radius;
  const x = cx + radius * Math.cos(angle * (Math.PI / 180));
  const y = cy + radius * Math.sin(angle * (Math.PI / 180));
  return [ x, y ];
};

export const getRotationOffsets = (numSegs) => {
  /**
   * TODO: there is an obvious negative exponential curve here
   * There should be a formulat function to return these values
   */
  switch (numSegs) {
    case 2:
      return [ 90, 45 ]; // 45
    case 3:
      return [ 120, 16 ]; // 16
    case 4:
      return [ 135, 0 ]; // 0
    case 5:
      return [ 144, -9 ]; // -10/9
    case 6:
      return [ 150, -15 ]; // -15
    case 7:
      return [ 157, -21 ]; // -20
    case 8:
      return [ 157, -21 ]; // -20
    default:
      return 0;
  }
};

const IconsBroken = ({ indicators }) => {
  const [ zRing, zIcon ] = getRotationOffsets(indicators.length);
  return (
    <div css={styles} style={{ transformOrigin: 'center', transform: `rotate(${zRing + rotation}deg)` }}>
      <div className="brokens-container">
        {indicators.map((data, i) => {
          if (data.state === 'broken') {
            const angle = Math.floor(360 / indicators.length) * i;
            const [ x, y ] = getRingCoords(angle);
            return (
              <img
                style={{
                  position: 'absolute',
                  transformOrigin: 'center',
                  transform: `translate(${x}px,${y}px) rotate(${zIcon}deg) scale(1.15)`,
                }}
                src={iconBreak}
                key={i}
              />
            );
          }
        })}
      </div>
    </div>
  );
};

IconsBroken.propTypes = {
  indicators: PropTypes.array.isRequired,
};

export default IconsBroken;
