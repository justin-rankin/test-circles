import { css } from '@emotion/react';
import { diameterPX, adjustPX } from 'components/Circle/config';

export const styles = css`
  position: absolute;
  z-index: 30;
  width: ${diameterPX}px;
  height: ${diameterPX}px;

  .brokens-container {
    padding: 2px;
    position: absolute;
    width: ${diameterPX}px;
    height: ${diameterPX}px;
    transform-origin: 'center';
    transform: translateX(${adjustPX / 2}px) translateY(${adjustPX / 2}px);
  }
`;
