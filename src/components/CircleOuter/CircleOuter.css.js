import { css } from '@emotion/react';
import { diameterPX } from 'components/Circle/config';

export const styles = css`
  position: absolute;
  z-index: 10;
  .svg-container {
    width: ${diameterPX}px;
    transform: scaleX(-1); /* FIX SO THAT INDICATORS READ CLOCK-WISE */

    .outer-ring-bg {
      stroke: rgba(200, 200, 200, 0.5);
    }
  }
`;
