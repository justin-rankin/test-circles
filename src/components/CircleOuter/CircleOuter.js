/** @jsx jsx */
import PropTypes from 'prop-types';
import OuterMask from './svgElements/OuterMask';
import OuterRing from './svgElements/OuterRing';
import IconsBroken from './components/IconsBroken';
import { jsx } from '@emotion/react';
import { styles } from './CircleOuter.css';

import { diameter, getPropsOuterMask, getPropsOuterRing } from 'components/Circle/config';

const CircleOuter = ({ indicators }) => {
  const propsOuterMask = getPropsOuterMask(indicators);
  const propsOuterRing = getPropsOuterRing(indicators);

  return (
    <div css={styles} className="circe-outer">
      <IconsBroken indicators={indicators} />
      <svg width="100%" height="100%" viewBox={`0 0 ${diameter} ${diameter}`} className="svg-container">
        <OuterRing {...propsOuterRing} />
        <OuterMask {...propsOuterMask} />
      </svg>
    </div>
  );
};

CircleOuter.propTypes = {
  indicators: PropTypes.array.isRequired,
};

export default CircleOuter;
