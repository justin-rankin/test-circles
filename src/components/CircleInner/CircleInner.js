/** @jsx jsx */
import PropTypes from 'prop-types';

import InnerMask from './svgElements/InnerMask';
import InnerCircle from './svgElements/InnerCircle';
import InnerTitle from './components/InnerTitle';
import InnerButtons from './components/InnerButtons';

import iconView from 'assets/iconMagnifier.svg';
import iconVisit from 'assets/iconVisitArrow.svg';

import { jsx } from '@emotion/react';
import { styles } from './CircleInner.css';

import {
  diameter,
  diameterPX,
  propsInnerMask,
  propsInnerCircle,
  propsInnerTitle,
  propsInnerSVG
} from 'components/Circle/config';

const CircleInner = ({ title }) => {
  return (
    <div css={styles} className="circe-inner">
      <img
        src={iconView}
        alt="VIEW"
        className="icon icon-view"
        style={{ transform: `translate(${diameterPX * 0.31}px, ${diameterPX * 0.61}px) scale(1.1)` }}
      />
      <img
        src={iconVisit}
        alt="VIEW"
        className="icon icon-visit"
        style={{ transform: `translate(${diameterPX * 0.6}px, ${diameterPX * 0.6}px)` }}
      />
      <svg width="100%" height="100%" viewBox={`0 0 ${diameter} ${diameter}`} className="svg-container">
        <InnerMask {...propsInnerMask} />
        <InnerCircle {...propsInnerCircle} />
        <InnerTitle title={title} {...propsInnerTitle} />
        <InnerButtons propsInnerSVG={propsInnerSVG} />
      </svg>
    </div>
  );
};

CircleInner.propTypes = {
  title: PropTypes.string.isRequired,
};

export default CircleInner;
