/** @jsx jsx */
import PropTypes from 'prop-types';
import { jsx } from '@emotion/react';
import { styles } from './InnerButtons.css';

import { circumference, diameter, rValue, ringWidth, innerScale } from 'components/Circle/config';

const InnerButtons = ({ propsInnerSVG }) => {
  return (
    <g css={styles}>
      <g mask="url(#maskInnerBreaks)">
        <circle
          className="inner-element-bg btn btn-view"
          {...propsInnerSVG}
          strokeDasharray={`${25 * innerScale} ${25 * innerScale} 0 ${circumference - 25 * innerScale}`}
          strokeDashoffset={-25 * innerScale}
          onPointerDown={() => alert('clicked: VIEW')}
        />
        <circle
          className="inner-element-bg btn btn-visit"
          {...propsInnerSVG}
          strokeDasharray={`${25 * innerScale} ${25 * innerScale} 0 ${circumference - 25 * innerScale}`}
          strokeDashoffset={0}
          onPointerDown={() => alert('clicked: VISIT')}
        />
        <circle
          cx={`${diameter / 2}`}
          cy={`${diameter / 2}`}
          r={rValue * 1.1}
          stroke="transparent"
          fill="none"
          strokeWidth={ringWidth * 2}
          className="btn-mask"
        />
      </g>
    </g>
  );
};

InnerButtons.propTypes = {
  propsInnerSVG: PropTypes.object.isRequired,
};

export default InnerButtons;
