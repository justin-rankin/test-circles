/** @jsx jsx */
import PropTypes from 'prop-types';
import { jsx } from '@emotion/react';
import { styles } from './InnerTitle.css';

const InnerTitle = ({ title, diameter, ringPadding, rValue, innerScale, propsInnerSVG }) => {
  return (
    <g css={styles}>
      <g mask="url(#maskInnerBreaks)">
        <circle
          className="inner-element-bg"
          {...propsInnerSVG}
          strokeDasharray={`${50 * innerScale} 0 0 ${50 * innerScale}`}
          strokeDashoffset={50 * innerScale}
        />
      </g>

      <g className="circle-title">
        <path
          id="curve"
          fill="rgba(0,0,0,0)"
          d={`
					M ${diameter * 0.42}, ${diameter / 1.15}
					a 10,12 1 1,1 ${diameter / 3},0`}
        />
        <text dy="0px" textAnchor="middle">
          <textPath href="#curve" startOffset="50%">
            {title}
          </textPath>
        </text>
      </g>
    </g>
  );
};

InnerTitle.propTypes = {
  title: PropTypes.string.isRequired,
  diameter: PropTypes.number,
  ringPadding: PropTypes.number,
  rValue: PropTypes.number,
  innerScale: PropTypes.number,
  propsInnerSVG: PropTypes.object.isRequired,
};

export default InnerTitle;
