import { css } from '@emotion/react';

export const styles = css`
  .circle-title {
    color: white;
    font-size: 0.25rem;
    transform-origin: 'center';
    transform: scale(0.85) translateY(1px);
  }
`;
