import { css } from '@emotion/react';

export const styles = css`
  .btn {
    cursor: pointer;
    pointer-events: visibleStroke;
    transition: stroke 300ms ease;
    &:hover {
      stroke: rgba(255, 255, 255, 0.5) !important;
      /* TODO: remove !important */
    }
  }
`;
