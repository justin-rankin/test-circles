/** @jsx jsx */
import PropTypes from 'prop-types';
import { jsx } from '@emotion/react';

const InnerCircle = ({ diameter, rValue, ringPadding }) => {
  return (
    <circle className="inner-circle-bg" cx={`${diameter / 2}`} cy={`${diameter / 2}`} r={rValue - ringPadding * 0.9} />
  );
};

InnerCircle.propTypes = {
  diameter: PropTypes.number.isRequired,
  rValue: PropTypes.number.isRequired,
  ringPadding: PropTypes.number.isRequired,
};

export default InnerCircle;
