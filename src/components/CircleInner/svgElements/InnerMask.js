/** @jsx jsx */
import PropTypes from 'prop-types';
import { jsx } from '@emotion/react';

const InnerMask = ({ diameter, rValue, innerScale, propsSpacingMaskSVG }) => {
  return (
    <mask id="maskInnerBreaks">
      {<circle cx={`${diameter / 2}`} cy={`${diameter / 2}`} r={rValue * innerScale} fill="white" />}
      <polyline {...propsSpacingMaskSVG} style={{ transformOrigin: 'center', transform: 'rotate(0deg)' }} />
      <polyline {...propsSpacingMaskSVG} style={{ transformOrigin: 'center', transform: 'rotate(90deg)' }} />
      <polyline {...propsSpacingMaskSVG} style={{ transformOrigin: 'center', transform: 'rotate(180deg)' }} />
    </mask>
  );
};

InnerMask.propTypes = {
  diameter: PropTypes.number.isRequired,
  rValue: PropTypes.number.isRequired,
  innerScale: PropTypes.number.isRequired,
  propsSpacingMaskSVG: PropTypes.object.isRequired,
};

export default InnerMask;
