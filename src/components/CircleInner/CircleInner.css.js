import { css } from '@emotion/react';
import { diameterPX } from 'components/Circle/config';

export const styles = css`
  position: absolute;
  z-index: 20;

  .svg-container {
    width: ${diameterPX}px;

    .inner-circle-bg {
      fill: rgba(0, 0, 0, 0.15);
    }

    .inner-element-bg {
      stroke: rgba(255, 255, 255, 0.25);
    }
  }

  .icon {
    position: absolute;
    pointer-events: none;
  }
`;
