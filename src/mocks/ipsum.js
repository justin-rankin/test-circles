import getRandomInt from './getRandomInt';

// MOCK INDICATORS + STATES
export const ipsum =
  'Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur Excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit anim id est laborum';

function titleCase(str) {
  str = str.toLowerCase().split(' ');
  for (let i = 0; i < str.length; i++) {
    str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1);
  }
  return str.join(' ');
}

export const getIpsumTitle = () => {
  const stringLen = getRandomInt(4, 12);
  const start = getRandomInt(0, ipsum.length - stringLen);
  const randomString = ipsum.substr(start, stringLen);
  return titleCase(randomString).trim();
};
