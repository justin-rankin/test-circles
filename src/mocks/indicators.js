import getRandomInt from './getRandomInt';
// MOCK INDICATORS + STATES
const LIT = 'lit';
const UNLIT = 'unlit';
const BROKEN = 'broken';

const states = [
  LIT,
  UNLIT,
  BROKEN, 
];

export const getRandomIndicators = (min, max) => {
  const getRandomIndicators = [];
  const arrLength = getRandomInt(min, max);
  for (let i = 0; i < arrLength; i++) {
    const rndState = states[getRandomInt(0, states.length - 1)];
    getRandomIndicators.push({ state: rndState });
  }
  return getRandomIndicators;
};
