# 📒 **Circles - Frontend Technical Test**

**Basic React boilerplate app built with:**

- React: react-app-rewired + customize-cra
- emotion CSS-in-JS
- eslint
- babel

**Test instructions:**
Your challenge is to implement that UI as closely as possible in html/css in any framework you prefer. Email us a URL, or a file that we can easily view.

![instructions](src/assets/Circle%20Entry.png)

**Launch test:**

```sh
yarn
yarn start
```

or

```sh
npm install
npm run start
```

When DEMO is running, click the "Randomize!" button to change the state (title, indicators array -both number/length and state of each), rendering a new random version of the Circle component.

**NOTES:**

- As instructions say "number of indicators varies per Circle", I created a React component that takes an `indicators` array as props arg and renders the indicators dynamically.
- A `title` string props arg also sets the circle title.
- There is a possibly better approach I could have taken, described below.  I started out with the current approach with the intent on making the component as dynamic as possible, but would probably prefer to incorperate some of the alternative ideas with a refactor.

**TODO**: future iterations would include:

- An alternate, method to dynamically drawing the indicators, would be to have seperate Indicator components with preset sizes: 1/8, 1/7, 1/6, 1/5, 1/4, 1/3, 1/2,  depending on the number of indicators.  The indicator segments could then be repeated, rotating around to complete the circle. This method, though not seemily more static, would perhaps have been easier and better.
- Seperate Indicator components would also allow for embedding the "broken" icon directly inside the component (currently not possible due to the current Indicator component returning internal SVG contents).
- Local React state for each Indicator using `const [ state, setState ] = useState()`
- seperate inner buttons as components, each containing it's icon (Again, currently not possible due to the current component returning internal SVG contents).

---

*Submitted by* **Justin Rankin**
[justin.blair.rankin@gmail.com](justin.blair.rankin@gmail.com)